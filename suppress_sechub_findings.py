import boto3


def create_session(account_number):
    sts = boto3.client('sts')
    role_arn = "arn:aws:iam::"+account_number + \
        ":role/CpeTest2"

    response = sts.assume_role(
        RoleArn=role_arn,
        RoleSessionName="SecHub"
    )

    credentials = response['Credentials']
    assumed_role_session = boto3.Session(
        aws_access_key_id=credentials["AccessKeyId"],
        aws_secret_access_key=credentials["SecretAccessKey"],
        aws_session_token=credentials["SessionToken"],
    )

    return assumed_role_session




def get_sechub_finding(assumed_role_session):
    security_hub_client = assumed_role_session.client(
        'securityhub', region_name='us-east-1')

    response = security_hub_client.get_findings(
        Filters={
            "GeneratorId": [
                {
                    "Value": "aws-foundational-security-best-practices/v/1.0.0/IAM.6",
                    "Comparison": "PREFIX"
                }
            ],
            "RecordState": [
                {
                    "Value": "ACTIVE",
                    "Comparison": "EQUALS"
                }
            ]
        }
    )

    finding_arn = response["Findings"][0]["Id"]
    return finding_arn


def suppress_finding(assumed_role_session,finding_arn):
    security_hub_client = assumed_role_session.client(
        'securityhub', region_name='us-east-1')
    finding_response = security_hub_client.batch_update_findings(
        FindingIdentifiers=[
            {
                'Id': finding_arn,
                'ProductArn': "arn:aws:securityhub:us-east-1::product/aws/securityhub"
            },
        ],
        Workflow={
            'Status': 'SUPPRESSED'
        }
    )
    if finding_response['UnprocessedFindings']:
        print("Finding Not Found")
    else:
        print("Successful")


reader = open("accounts.txt", "r", encoding='utf-8-sig')
accounts = reader.read().splitlines()

assumed_role_session = create_session("582179419329")
sechub_finding = get_sechub_finding(assumed_role_session)
suppress_finding(assumed_role_session,sechub_finding)